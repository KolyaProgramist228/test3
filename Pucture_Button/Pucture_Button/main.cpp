#include <windows.h>
#include <TChar.h>
#include "resource.h"
HINSTANCE hInstance;
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(
		HINSTANCE hInstance,    // ���������� �������� ���������� ����������
		HINSTANCE hPrevInstance,// ���������� ����������� ���������� ����������
		LPSTR lpCmdLine,        // ��������� �� ��������� ������
		int nCmdShow = SW_SHOWMINIMIZED           // ���������� ��������� ����
)
{
	MSG uMsg; 
	HWND hMainWnd;//��������� �� �������� ���� 
	TCHAR szClassName[] = L"MyClass"; // �������� ����� ������ ����
	
	WNDCLASSEX wcex; // ���������� ���������� ���� ��������� �������� ������

	wcex.cbSize         = sizeof(WNDCLASSEX); // ������ ��������� � ������
	wcex.style		    = CS_HREDRAW | CS_VREDRAW;	// ����� ����
	wcex.lpfnWndProc	= (WNDPROC)WndProc; //����� ������� ���������
	wcex.cbClsExtra		= 0;						
	wcex.cbWndExtra	    = 0;
	wcex.hInstance		= hInstance;		// ��������� ����������
	wcex.hIcon		    = LoadIcon(hInstance,MAKEINTRESOURCE(IDI_APPLICATION));		// ����������� ������
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW); // ����������� �������
	wcex.hbrBackground	= GetSysColorBrush(COLOR_BTNFACE); // ��������� ����
	wcex.lpszMenuName	= NULL;		// ����������� ����
	wcex.lpszClassName	= szClassName;	// ��� ������
	wcex.hIconSm		= NULL; //����������� ��������� ������

	

	// ������������ ����� ���� 
	if (!RegisterClassEx(&wcex)) { 
		MessageBox(NULL, L"Cannot register class", L"Error", MB_OK); 
		return 0; 
	}
	
	// ������� �������� ���� ���������� 
	hMainWnd = CreateWindow( 
	szClassName, L"A Hellol Application", WS_OVERLAPPEDWINDOW, 
	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
	(HWND)NULL, (HMENU)NULL, 
	(HINSTANCE)hInstance, NULL 
	); 

	// ���������� ���� ���� 
	ShowWindow(hMainWnd, nCmdShow); 
	// UpdateWindow(hMainWnd); // ��. ���������� � ������� "����������� ����.. 

	// ��������� ���� ��������� ��������� �� �������� ���������� 
	while (GetMessage(&uMsg, NULL, 0, 0)) { 
	TranslateMessage(&uMsg); 
	DispatchMessage(&uMsg); 
	}
	return 0;
}

HWND hButton = NULL;
HBITMAP hBm1 = NULL, hBm2 = NULL, hBm3 = NULL;
int nPicID = 0;
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{ 
    PAINTSTRUCT ps; 
    LOGBRUSH lb; 
    RECT rc; 
    HDC hdc; 
	HINSTANCE hInst;
	BOOL flag = false;
	hInst = hInstance;
    switch (uMsg) 
    { 
	case WM_CREATE:
		{
			hBm1 = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP1));
			hBm2 = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP2));
			hBm3 = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP3));

			hButton = CreateWindow (L"button", L"EDIT", 
				WS_CHILD | WS_VISIBLE |BS_PUSHBUTTON | BS_BITMAP, 10, 10, 100, 30, 
				hWnd, (HMENU) 1, hInst, NULL);

			SendMessage(hButton, BM_SETIMAGE, IMAGE_BITMAP,(LPARAM)hBm1);

			nPicID = 1;
		}
		break;
        case WM_PAINT: 
        { 
			hdc=BeginPaint(hWnd,&ps);
			
            EndPaint(hWnd, &ps); 
 
        } 
        break; 
		case WM_COMMAND:
		{	
			int c;
			HWND hCommWnd;
			int ID;

			//������ � ���������� �������� �������������� ���������
			ID = LOWORD( wParam ) ;//�������������
			hCommWnd = HWND ( lParam );//��������� ����\������, �� �-��� ������ ���������
			c = HIWORD (wParam);//�����������

			switch ( ID )
			{
			case 1:
				{
					if(c == BN_CLICKED){
						//MessageBox(NULL, L"", L"", MB_OK);
						switch (nPicID){
							case 1:{
								nPicID = 2;
								SendMessage(hButton, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBm2);
							}break;
							case 2:{
								nPicID = 3;
								SendMessage(hButton, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBm3);
							}break;
							case 3:{
								nPicID = 1;
								SendMessage(hButton, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBm1);
							}break;
						}
					}
				}
				break;
			}
			
		}
			break;
         case WM_DESTROY: 
            PostQuitMessage(0); 
            break; 
 
        default: 
            return DefWindowProc(hWnd, uMsg, wParam, lParam); 
    } 
 
    return FALSE; 
}